FROM ubuntu:trusty

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install wget \
                       openjdk-7-jre \
                       firefox
RUN apt-get clean

ENV USER soapuiuser
ENV VERSION 5.2.1
ENV HOME /home/${USER}
RUN useradd --create-home --home-dir ${HOME} ${USER} \
	&& chown -R ${USER}:${USER} ${HOME}

WORKDIR /opt

RUN wget -O SoapUI-${VERSION}-linux-bin.tar.gz http://cdn01.downloads.smartbear.com/soapui/${VERSION}/SoapUI-${VERSION}-linux-bin.tar.gz
RUN tar xvfz SoapUI-${VERSION}-linux-bin.tar.gz
RUN rm SoapUI-${VERSION}-linux-bin.tar.gz

RUN chown -R ${USER}:${USER} SoapUI-${VERSION}

WORKDIR ${HOME}
USER ${USER}

ENTRYPOINT /opt/SoapUI-${VERSION}/bin/soapui.sh
